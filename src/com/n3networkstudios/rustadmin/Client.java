package com.n3networkstudios.rustadmin;

import java.awt.Dimension;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.WindowConstants;

import org.jnativehook.GlobalScreen;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

public class Client implements NativeKeyListener {
	public static boolean hasInit, panelOpen, consoleOpen;
	public static JFrame frame;
	public static Robot robot;
	public static JList<Object> list;
	public static WindowOption[] options;
	public static String backMenu, username;
	public static LinkedList<String> itemsList;

	public void init() {
		try {
			GlobalScreen.registerNativeHook();
			GlobalScreen.getInstance().addNativeKeyListener(this);
			robot = new Robot();
			createMenu("setup");
			itemsList = new LinkedList<String>();
			BufferedReader br = null;
			try {
				br = new BufferedReader(new FileReader(new File("item.list")));
				String line;
				while ((line = br.readLine()) != null) {
					itemsList.add(line);
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					br.close();
				} catch (Exception e) {
				}
			}
			try {
				br = new BufferedReader(new FileReader(new File("option.list")));
				username = br.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					br.close();
				} catch (Exception e) {
				}
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Sorry, unable to start app",
					"Rust app: ERROR", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}

	public String remove0Arror(String s) {
		if (s.indexOf(">") == 0)
			return s.substring(1);
		return s;
	}

	@Override
	public void nativeKeyPressed(NativeKeyEvent e) {
		if (e.getKeyCode() == NativeKeyEvent.VK_F4) {
			exit();
		} else if ((!hasInit || panelOpen)
				&& e.getKeyCode() == NativeKeyEvent.VK_UP) {
			if (list.getSelectedIndex() > 0) {
				list.ensureIndexIsVisible(list.getSelectedIndex() - 1);
				list.setSelectedIndex(list.getSelectedIndex() - 1);
			}
		} else if ((!hasInit || panelOpen)
				&& e.getKeyCode() == NativeKeyEvent.VK_DOWN) {
			if (list.getSelectedIndex() < options.length) {
				list.ensureIndexIsVisible(list.getSelectedIndex() + 1);
				list.setSelectedIndex(list.getSelectedIndex() + 1);
			}
		} else if ((!hasInit || panelOpen)
				&& e.getKeyCode() == NativeKeyEvent.VK_RIGHT) {
			options[list.getSelectedIndex()].selected();
		} else if ((!hasInit || panelOpen)
				&& e.getKeyCode() == NativeKeyEvent.VK_LEFT) {
			if (backMenu != "")
				createMenu(backMenu);
		} else if (!hasInit) {
			if (e.getKeyCode() == NativeKeyEvent.VK_F3) {
				closeWindow();
				consoleOpen = true;
				doCommand("status");
				hasInit = true;
				panelOpen = false;
			}
		} else if (panelOpen) {
			if (e.getKeyCode() == NativeKeyEvent.VK_F2) {
				panelOpen = false;
				closeWindow();
			}
		} else {
			if (e.getKeyCode() == NativeKeyEvent.VK_F2) {
				panelOpen = true;
				createMenu("home");
			}
		}
	}

	public void createMenu(String menu) {
		System.out.println("create " + menu);
		if (menu == "setup") {
			backMenu = "";
			createWindow(
					"Setup",
					new WindowOption[] {
							new WindowOption(
									"Welcome to the n3network studios client app"),
							new WindowOption(
									"Please go into rust and open console(f1) and press"),
							new WindowOption(
									"f3 to activate the interface with the game"),
							new WindowOption(
									"f2 once activated will open and close the menu"),
							new WindowOption(
									"f4 will will exit this app at anytime"),
							new WindowOption("Exit") {
								@Override
								public void selected() {
									exit();
								};
							} });
		} else if (menu == "home") {
			backMenu = "";
			createWindow("Home", new WindowOption[] {
					new WindowOption("Client") {
						@Override
						public void selected() {
							createMenu("home>client");
						}
					}, new WindowOption("Admin") {
						@Override
						public void selected() {
							createMenu("home>admin");
						}
					}, new WindowOption("Options") {
						@Override
						public void selected() {
							createMenu("home>options");
						};
					} });
		} else if (menu == "home>client") {
			backMenu = "home";
			createWindow("Home > Client", new WindowOption[] {
					new WindowOption("Grass") {
						@Override
						public void selected() {
							createMenu("home>client>grass");
						}
					}, new WindowOption("Gui") {
						@Override
						public void selected() {
							createMenu("home>client>gui");
						}
					}, new WindowOption("Network") {
						@Override
						public void selected() {
							createMenu("home>client>net");
						}
					} });
		} else if (menu == "home>client>grass") {
			backMenu = "home>client";
			createWindow("Home > Client > Grass", new WindowOption[] {
					new WindowOption("Show") {
						@Override
						public void selected() {
							doCommand("grass.on true");
						}
					}, new WindowOption("Hide") {
						@Override
						public void selected() {
							doCommand("grass.on false");
						}
					} });
		} else if (menu == "home>client>gui") {
			backMenu = "home>client";
			createWindow("Home > Client > Gui", new WindowOption[] {
					new WindowOption("Show") {
						@Override
						public void selected() {
							doCommand("gui.show");
						}
					}, new WindowOption("Hide") {
						@Override
						public void selected() {
							doCommand("gui.hide");
						}
					} });
		} else if (menu == "home>client>net") {
			backMenu = "home>client";
			createWindow("Home > Client > Net", new WindowOption[] {
					new WindowOption("Join") {
						@Override
						public void selected() {
							createMenu("home>client>net>join");
						}
					}, new WindowOption("Reconnect") {
						@Override
						public void selected() {
							doCommand("net.disconnect");
							doCommand("net.reconnect");
						}
					} });
		} else if (menu == "home>client>net>join") {
			backMenu = "home>client>net>join";
			LinkedList<WindowOption> options = new LinkedList<WindowOption>();
			options.add(new WindowOption("Ip") {
				@Override
				public void selected() {
					doCommand("net.connect "
							+ getInput("Rust app: Client join", "Server ip"));
				}
			});
			options.add(new WindowCommand("Join last", "net.reconnect"));
			BufferedReader br = null;
			try {
				br = new BufferedReader(new FileReader(new File("server.list")));
				String line;
				while ((line = br.readLine()) != null) {
					String[] p = line.split("~");
					options.add(new WindowCommand(p[0], "net.connect " + p[1]));
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					br.close();
				} catch (Exception e) {
				}
			}
			// Example
			createWindow("Home > Client > Net > Join",
					options.toArray(new WindowOption[options.size()]));
		} else if (menu == "home>admin") {
			backMenu = "home";
			createWindow("Home > Admin", new WindowOption[] {
					new WindowOption("Login") {
						@Override
						public void selected() {
							doCommand("rcon.login "
									+ getInput("Rust app: Admin login",
											"RCon Password"));
						}
					}, new WindowOption("Spawn") {
						@Override
						public void selected() {
							createMenu("home>admin>spawn");
						}
					}, new WindowOption("Teleport") {
						@Override
						public void selected() {
							createMenu("home>admin>teleport");
						}
					}, new WindowOption("Player") {
						@Override
						public void selected() {
							createMenu("home>admin>player");
						}
					}, new WindowOption("Airdrops") {
						@Override
						public void selected() {
							createMenu("home>admin>airdrop");
						}
					}, new WindowOption("Crafting") {
						@Override
						public void selected() {
							createMenu("home>admin>crafting");
						}
					}, new WindowOption("Utils") {
						@Override
						public void selected() {
							createMenu("home>admin>util");
						}
					}, new WindowOption("Settings") {
						@Override
						public void selected() {
							createMenu("home>admin>settings");
						}
					} });
		} else if (menu == "home>admin>airdrop") {
			backMenu = "home>admin";
			createWindow("Home > Admin > Airdrops", new WindowOption[] {
					new WindowOption("Drop one") {
						@Override
						public void selected() {
							doCommand("airdrop.drop");
						}
					}, new WindowOption("Drop ?") {
						@Override
						public void selected() {
							if (!consoleOpen)
								toggleConsole();
							int tot = Integer.parseInt(getInput(
									"Rust app: Airdrops", "Ammount to drop"));
							for (int i = 0; i < tot; i++) {
								robot.delay(50);
								typeCommand("airdrop.drop");
								robot.delay(50);
							}
							toggleConsole();
						}
					}, new WindowOption("Drop min") {
						@Override
						public void selected() {
							doCommand("airdrop.min_players \""
									+ getInput("Rust app: Airdrops",
											"Required ammount of players for airdrops")
									+ "\"");
						}
					} });
		} else if (menu == "home>admin>settings") {
			backMenu = "home>admin";
			createWindow("Home > Admin > Settings", new WindowOption[] {
					new WindowOption("Hostname") {
						@Override
						public void selected() {
							doCommand("server.hostname "
									+ getInput("Rust app: Admin", "Hostname"));
						}
					}, new WindowOption("Timeout") {
						@Override
						public void selected() {
							doCommand("server.clienttimeout \""
									+ getInput("Rust app: Admin",
											"Client timeout") + "\"");
						}
					}, new WindowOption("Sleeper") {
						@Override
						public void selected() {
							createMenu("home>admin>settings>sleeper");
						}
					}, new WindowOption("PVP") {
						@Override
						public void selected() {
							createMenu("home>admin>settings>pvp");
						}
					}, new WindowOption("Force truth") {
						@Override
						public void selected() {
							createMenu("home>admin>settings>truth");
						}
					} });
		} else if (menu == "home>admin>settings>pvp") {
			backMenu = "home>admin>settings";
			createWindow("Home > Admin > Settings > PVP", new WindowOption[] {
					new WindowOption("On") {
						@Override
						public void selected() {
							doCommand("server.pvp true");
						}
					}, new WindowOption("Off") {
						@Override
						public void selected() {
							doCommand("server.pvp false");
						}
					} });
		} else if (menu == "home>admin>settings>sleeper") {
			backMenu = "home>admin>settings";
			createWindow("Home > Admin > Settings > Sleeper",
					new WindowOption[] { new WindowOption("On") {
						@Override
						public void selected() {
							doCommand("sleepers.on true");
						}
					}, new WindowOption("Off") {
						@Override
						public void selected() {
							doCommand("sleepers.on false");
						}
					} });
		} else if (menu == "home>admin>settings>truth") {
			backMenu = "home>admin>settings";
			createWindow("Home > Admin > Settings > Force truth",
					new WindowOption[] { new WindowOption("On") {
						@Override
						public void selected() {
							doCommand("truth.enforce true");
						}
					}, new WindowOption("Off") {
						@Override
						public void selected() {
							doCommand("truth.enforce false");
						}
					} });
		} else if (menu == "home>admin>util") {
			backMenu = "home>admin>";
			createWindow("Home > Admin > Util", new WindowOption[] {
					new WindowOption("Save") {
						@Override
						public void selected() {
							doCommand("save.all");
						}
					}, new WindowOption("Admin godmode") {
						@Override
						public void selected() {
							createMenu("home>admin>util>godmode");
						}
					} });
		} else if (menu == "home>admin>util>godmode") {
			backMenu = "home>admin>util";
			createWindow("Home > Admin > Util > God mode", new WindowOption[] {
					new WindowOption("On") {
						@Override
						public void selected() {
							doCommand("dmg.godmode true");
						}
					}, new WindowOption("Off") {
						@Override
						public void selected() {
							doCommand("dmg.godmode false");
						}
					} });
		} else if (menu == "home>admin>player") {
			backMenu = "home>admin";
			createWindow("Home > Admin > Player", new WindowOption[] {
					new WindowOption("Kick") {
						@Override
						public void selected() {
							doCommand("kick \""
									+ getInput("Rust app: Kick",
											"Player username") + "\"");
						}
					}, new WindowOption("Ban") {
						@Override
						public void selected() {
							createMenu("home>admin>player>ban");
						}
					}, new WindowOption("Unban") {
						@Override
						public void selected() {
							createMenu("home>admin>player>unban");
						}
					} });
		} else if (menu == "home>admin>player>ban") {
			backMenu = "home>admin>player";
			createWindow("Home > Admin > Player > Ban", new WindowOption[] {
					new WindowOption("Username") {
						@Override
						public void selected() {
							doCommand("ban \""
									+ getInput("Rust app: Ban",
											"Player username") + "\"");
						}
					}, new WindowOption("Steam Id") {
						@Override
						public void selected() {
							doCommand("banid \""
									+ getInput("Rust app: Ban", "Steam Id")
									+ "\"");
						}
					} });
		} else if (menu == "home>admin>player>unban") {
			backMenu = "home>admin>player";
			createWindow("Home > Admin > Player > UnBan",
					new WindowOption[] { new WindowOption("All") {
						@Override
						public void selected() {
							doCommand("unbanall");
						}
					} });
		} else if (menu == "home>admin>teleport") {
			backMenu = "home>admin";
			createWindow("Home > Admin > Teleport", new WindowOption[] {
					new WindowOption("Me") {
						@Override
						public void selected() {
							createMenu("home>admin>teleport>me");
						}
					}, new WindowOption("Other") {
						@Override
						public void selected() {
							createMenu("home>admin>teleport>other");
						}
					} });
		} else if (menu == "home>admin>teleport>me") {
			backMenu = "home>admin>teleport";
			createWindow("Home > Admin > Teleport > Me", new WindowOption[] {
					new WindowOption("To player") {
						@Override
						public void selected() {
							doCommand("teleport.toplayer \""
									+ username
									+ "\" \""
									+ getInput("Rust app: Teleport",
											"To player") + "\"");
						}
					}, new WindowOption("To coords") {
						@Override
						public void selected() {
							doCommand("teleport.toplayer \""
									+ username
									+ "\" "
									+ getInput("Rust app: Teleport",
											"To coords (x y z)") + "");
						}
					} });
		} else if (menu == "home>admin>teleport>other") {
			backMenu = "home>admin>teleport";
			createWindow("Home > Admin > Teleport > Other", new WindowOption[] {
					new WindowOption("To me") {
						@Override
						public void selected() {
							doCommand("teleport.toplayer \""
									+ getInput("Rust app: Teleport", "Player")
									+ "\" \"" + username + "\"");
						}
					}, new WindowOption("To player") {
						@Override
						public void selected() {
							doCommand("teleport.toplayer \""
									+ getInput("Rust app: Teleport", "Player 1")
									+ "\" "
									+ getInput("Rust app: Teleport", "Player 2")
									+ "");
						}
					}, new WindowOption("To coords") {
						@Override
						public void selected() {
							doCommand("teleport.toplayer \""
									+ getInput("Rust app: Teleport", "Player")
									+ "\" "
									+ getInput("Rust app: Teleport",
											"To coords (x y z)") + "");
						}
					} });
		} else if (menu.equals("home>admin>spawn")) {
			backMenu = "home>admin";
			createWindow("Home > Admin > Spawn", new WindowOption[] {
					new WindowOption("Items") {
						@Override
						public void selected() {
							createMenu("home>admin>spawn>items");
						}
					}, new WindowOption("Vehicle") {
						@Override
						public void selected() {
							doCommand("vehicle.spawn");
						}
					} });
		} else if (menu.startsWith("home>admin>spawn>items")) {
			backMenu = menu.substring(0, menu.lastIndexOf(">"));
			System.out.println(menu + "#" + backMenu);
			String cat = remove0Arror(menu
					.replace("home>admin>spawn>items", ""));
			LinkedList<String> areas = new LinkedList<String>();
			LinkedList<String> items = new LinkedList<String>();
			for (String i : itemsList) {
				String[] b = i.split("~");
				if (b[0].equals(cat)) {
					String name = b[0].lastIndexOf(">") == -1 ? b[0] : b[0]
							.substring(b[0].lastIndexOf(">") + 1);
					int max = Integer.parseInt(b[1]);
					items.add(name + "~" + max);
				} else if (b[0].contains(cat)) {
					String catRem = remove0Arror(b[0].substring(cat.length()));
					String name = catRem.indexOf(">") == -1 ? catRem : catRem
							.substring(0, catRem.indexOf(">"));
					if (!areas.contains(name)) {
						areas.add(name);
					}
				}
			}
			LinkedList<WindowOption> options = new LinkedList<WindowOption>();
			for (String a : areas) {
				options.add(new WindowMenu(a, menu + ">" + a));
			}
			for (String i : items) {
				String[] bits = i.split("~");
				int max = Integer.parseInt(bits[1]);
				options.add(new WindowOption(bits[0]));
				options.add(new WindowItem(" Me x1", username, bits[0], 1));
				options.add(new WindowItem(" Other x1", "ask", bits[0], 1));
				options.add(new WindowItem(" All x1", "all", bits[0], 1));
				if (max > 1) {
					options.add(new WindowItem(" Me x" + max, username,
							bits[0], max));
					options.add(new WindowItem(" Other x" + max, "ask",
							bits[0], max));
					options.add(new WindowItem(" All x" + max, "all", bits[0],
							max));
				}
				options.add(new WindowItem(" Me x?", username, bits[0], -1));
				options.add(new WindowItem(" Other x?", "ask", bits[0], -1));
				options.add(new WindowItem(" All x?", "all", bits[0], -1));
			}
			createWindow("Home > Admin > Spawn > Items > ?",
					options.toArray(new WindowOption[options.size()]));
		} else if (menu.equals("home>admin>crafting")) {
			backMenu = "home>admin";
			createWindow("Home > Admin > Crafting", new WindowOption[] {
					new WindowOption("Complete all") {
						@Override
						public void selected() {
							doCommand("crafting.complete");
						}
					}, new WindowOption("Cancel All") {
						@Override
						public void selected() {
							doCommand("crafting.cancel");
						}
					}, new WindowOption("Everybody instant crafting") {
						@Override
						public void selected() {
							createMenu("home>admin>crafting>instacraft");
						}
					}, new WindowOption("Admin instant crafting") {
						@Override
						public void selected() {
							createMenu("home>admin>crafting>admininstacraft");
						}
					}, new WindowOption("Timescale") {
						@Override
						public void selected() {
							doCommand("crafting.timescale \""
									+ getInput("Rust app: Crafting",
											"Timescale(1=default,0.5=half)")
									+ "\"");
						}
					} });
		} else if (menu.equals("home>admin>crafting>instacraft")) {
			backMenu = "home>admin>crafting";
			createWindow("Home > Admin > Crafting > Instacraft",
					new WindowOption[] { new WindowOption("On") {
						@Override
						public void selected() {
							doCommand("crafting.instant true");
						}
					}, new WindowOption("Off") {
						@Override
						public void selected() {
							doCommand("crafting.instant false");
						}
					} });
		}else if (menu.equals("home>admin>crafting>admininstacraft")) {
			backMenu = "home>admin>crafting";
			createWindow("Home > Admin > Crafting > Admin Instacraft",
					new WindowOption[] { new WindowOption("On") {
						@Override
						public void selected() {
							doCommand("crafting.instant true");
						}
					}, new WindowOption("Off") {
						@Override
						public void selected() {
							doCommand("crafting.instant false");
						}
					} });
		}
	}

	public String getInput(String title, String message) {
		return JOptionPane.showInputDialog(null, message, title,
				JOptionPane.QUESTION_MESSAGE);
	}

	public void doCommand(String command) {
		if (!consoleOpen)
			toggleConsole();
		robot.delay(50);
		typeCommand(command);
		robot.delay(50);
		toggleConsole();
	}

	public void typeCommand(String command) {
		type(command);
		robot.delay(50);
		doType(KeyEvent.VK_ENTER);
		robot.delay(50);
	}

	public void toggleConsole() {
		// robot.delay(100);
		consoleOpen = !consoleOpen;
		doType(KeyEvent.VK_F1);
		// robot.delay(100);
	}

	public void createWindow(String title, WindowOption[] options1) {
		closeWindow();
		options = options1;
		VerticalFlowLayout vfl = new VerticalFlowLayout();
		frame = new JFrame("Rust app: " + title);
		vfl.setHGap(2);
		vfl.setVGap(2);
		frame.setAlwaysOnTop(true);
		frame.setSize(320, 200);
		frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		frame.setFocusableWindowState(false);
		frame.setFocusable(false);

		JPanel panel = new JPanel(vfl);
		panel.add(new JLabel("up + down arrows = move select"));
		panel.add(new JLabel("left arrow = back - right arow = preform/next"));
		list = new JList<Object>(options);
		list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		list.setLayoutOrientation(JList.VERTICAL);
		list.setVisibleRowCount(-1);
		JScrollPane listScroller = new JScrollPane(list);
		listScroller.setPreferredSize(new Dimension(305, 123));
		list.setSelectedIndex(0);
		panel.add(listScroller);
		frame.add(panel);
		frame.setVisible(true);
	}

	public void closeWindow() {
		if (frame != null && frame.isVisible()) {
			frame.setVisible(false);
			frame.dispose();
		}
	}

	public void exit() {
		GlobalScreen.getInstance().removeNativeKeyListener(this);
		GlobalScreen.unregisterNativeHook();
		System.exit(0);
	}

	public static class WindowOption {
		private String name;

		public WindowOption(String name) {
			this.name = name;
		}

		public void selected() {
		}

		@Override
		public String toString() {
			return name;
		}
	}

	public class WindowCommand extends WindowOption {
		private String command;

		public WindowCommand(String name, String command) {
			super(name);
			this.command = command;
		}

		@Override
		public void selected() {
			doCommand(command);
		}
	}

	public class WindowMenu extends WindowOption {
		private String menu;

		public WindowMenu(String name, String menu) {
			super(name);
			this.menu = menu;
		}

		@Override
		public void selected() {
			createMenu(menu);
		}
	}

	public class WindowItem extends WindowOption {
		private String username, item;
		private int ammount;

		public WindowItem(String name, String username, String item, int ammount) {
			super(name);
			this.item = item;
			this.username = username;
			this.ammount = ammount;
		}

		@Override
		public void selected() {
			if (username == "ask")
				username = getInput("Rust app: Spawn", "Player");
			if (item == "ask")
				item = getInput("Rust app: Spawn", "Item");
			if (ammount == -1)
				ammount = Integer.parseInt(getInput("Rust app: Spawn",
						"Ammount"));
			if (username == "all") {
				doCommand("inv.giveall \"" + item + "\" \"" + ammount + "\"");
			} else {
				doCommand("inv.giveplayer \"" + username + "\" \"" + item
						+ "\" \"" + ammount + "\"");
			}
		}
	}

	@Override
	public void nativeKeyReleased(NativeKeyEvent e) {
	}

	@Override
	public void nativeKeyTyped(NativeKeyEvent e) {
	}

	public static void type(CharSequence characters) {
		int length = characters.length();
		for (int i = 0; i < length; i++) {
			char character = characters.charAt(i);
			type(character);
		}
	}

	public static void type(char character) {
		switch (character) {
		case 'a':
			doType(KeyEvent.VK_A);
			break;
		case 'b':
			doType(KeyEvent.VK_B);
			break;
		case 'c':
			doType(KeyEvent.VK_C);
			break;
		case 'd':
			doType(KeyEvent.VK_D);
			break;
		case 'e':
			doType(KeyEvent.VK_E);
			break;
		case 'f':
			doType(KeyEvent.VK_F);
			break;
		case 'g':
			doType(KeyEvent.VK_G);
			break;
		case 'h':
			doType(KeyEvent.VK_H);
			break;
		case 'i':
			doType(KeyEvent.VK_I);
			break;
		case 'j':
			doType(KeyEvent.VK_J);
			break;
		case 'k':
			doType(KeyEvent.VK_K);
			break;
		case 'l':
			doType(KeyEvent.VK_L);
			break;
		case 'm':
			doType(KeyEvent.VK_M);
			break;
		case 'n':
			doType(KeyEvent.VK_N);
			break;
		case 'o':
			doType(KeyEvent.VK_O);
			break;
		case 'p':
			doType(KeyEvent.VK_P);
			break;
		case 'q':
			doType(KeyEvent.VK_Q);
			break;
		case 'r':
			doType(KeyEvent.VK_R);
			break;
		case 's':
			doType(KeyEvent.VK_S);
			break;
		case 't':
			doType(KeyEvent.VK_T);
			break;
		case 'u':
			doType(KeyEvent.VK_U);
			break;
		case 'v':
			doType(KeyEvent.VK_V);
			break;
		case 'w':
			doType(KeyEvent.VK_W);
			break;
		case 'x':
			doType(KeyEvent.VK_X);
			break;
		case 'y':
			doType(KeyEvent.VK_Y);
			break;
		case 'z':
			doType(KeyEvent.VK_Z);
			break;
		case 'A':
			doType(KeyEvent.VK_SHIFT, KeyEvent.VK_A);
			break;
		case 'B':
			doType(KeyEvent.VK_SHIFT, KeyEvent.VK_B);
			break;
		case 'C':
			doType(KeyEvent.VK_SHIFT, KeyEvent.VK_C);
			break;
		case 'D':
			doType(KeyEvent.VK_SHIFT, KeyEvent.VK_D);
			break;
		case 'E':
			doType(KeyEvent.VK_SHIFT, KeyEvent.VK_E);
			break;
		case 'F':
			doType(KeyEvent.VK_SHIFT, KeyEvent.VK_F);
			break;
		case 'G':
			doType(KeyEvent.VK_SHIFT, KeyEvent.VK_G);
			break;
		case 'H':
			doType(KeyEvent.VK_SHIFT, KeyEvent.VK_H);
			break;
		case 'I':
			doType(KeyEvent.VK_SHIFT, KeyEvent.VK_I);
			break;
		case 'J':
			doType(KeyEvent.VK_SHIFT, KeyEvent.VK_J);
			break;
		case 'K':
			doType(KeyEvent.VK_SHIFT, KeyEvent.VK_K);
			break;
		case 'L':
			doType(KeyEvent.VK_SHIFT, KeyEvent.VK_L);
			break;
		case 'M':
			doType(KeyEvent.VK_SHIFT, KeyEvent.VK_M);
			break;
		case 'N':
			doType(KeyEvent.VK_SHIFT, KeyEvent.VK_N);
			break;
		case 'O':
			doType(KeyEvent.VK_SHIFT, KeyEvent.VK_O);
			break;
		case 'P':
			doType(KeyEvent.VK_SHIFT, KeyEvent.VK_P);
			break;
		case 'Q':
			doType(KeyEvent.VK_SHIFT, KeyEvent.VK_Q);
			break;
		case 'R':
			doType(KeyEvent.VK_SHIFT, KeyEvent.VK_R);
			break;
		case 'S':
			doType(KeyEvent.VK_SHIFT, KeyEvent.VK_S);
			break;
		case 'T':
			doType(KeyEvent.VK_SHIFT, KeyEvent.VK_T);
			break;
		case 'U':
			doType(KeyEvent.VK_SHIFT, KeyEvent.VK_U);
			break;
		case 'V':
			doType(KeyEvent.VK_SHIFT, KeyEvent.VK_V);
			break;
		case 'W':
			doType(KeyEvent.VK_SHIFT, KeyEvent.VK_W);
			break;
		case 'X':
			doType(KeyEvent.VK_SHIFT, KeyEvent.VK_X);
			break;
		case 'Y':
			doType(KeyEvent.VK_SHIFT, KeyEvent.VK_Y);
			break;
		case 'Z':
			doType(KeyEvent.VK_SHIFT, KeyEvent.VK_Z);
			break;
		case '`':
			doType(KeyEvent.VK_BACK_QUOTE);
			break;
		case '0':
			doType(KeyEvent.VK_0);
			break;
		case '1':
			doType(KeyEvent.VK_1);
			break;
		case '2':
			doType(KeyEvent.VK_2);
			break;
		case '3':
			doType(KeyEvent.VK_3);
			break;
		case '4':
			doType(KeyEvent.VK_4);
			break;
		case '5':
			doType(KeyEvent.VK_5);
			break;
		case '6':
			doType(KeyEvent.VK_6);
			break;
		case '7':
			doType(KeyEvent.VK_7);
			break;
		case '8':
			doType(KeyEvent.VK_8);
			break;
		case '9':
			doType(KeyEvent.VK_9);
			break;
		case '-':
			doType(KeyEvent.VK_MINUS);
			break;
		case '=':
			doType(KeyEvent.VK_EQUALS);
			break;
		case '~':
			doType(KeyEvent.VK_SHIFT, KeyEvent.VK_BACK_QUOTE);
			break;
		case '!':
			doType(KeyEvent.VK_EXCLAMATION_MARK);
			break;
		case '@':
			doType(KeyEvent.VK_AT);
			break;
		case '#':
			doType(KeyEvent.VK_NUMBER_SIGN);
			break;
		case '$':
			doType(KeyEvent.VK_DOLLAR);
			break;
		case '%':
			doType(KeyEvent.VK_SHIFT, KeyEvent.VK_5);
			break;
		case '^':
			doType(KeyEvent.VK_CIRCUMFLEX);
			break;
		case '&':
			doType(KeyEvent.VK_AMPERSAND);
			break;
		case '*':
			doType(KeyEvent.VK_ASTERISK);
			break;
		case '(':
			doType(KeyEvent.VK_LEFT_PARENTHESIS);
			break;
		case ')':
			doType(KeyEvent.VK_RIGHT_PARENTHESIS);
			break;
		case '_':
			doType(KeyEvent.VK_UNDERSCORE);
			break;
		case '+':
			doType(KeyEvent.VK_PLUS);
			break;
		case '\t':
			doType(KeyEvent.VK_TAB);
			break;
		case '\n':
			doType(KeyEvent.VK_ENTER);
			break;
		case '[':
			doType(KeyEvent.VK_OPEN_BRACKET);
			break;
		case ']':
			doType(KeyEvent.VK_CLOSE_BRACKET);
			break;
		case '\\':
			doType(KeyEvent.VK_BACK_SLASH);
			break;
		case '{':
			doType(KeyEvent.VK_SHIFT, KeyEvent.VK_OPEN_BRACKET);
			break;
		case '}':
			doType(KeyEvent.VK_SHIFT, KeyEvent.VK_CLOSE_BRACKET);
			break;
		case '|':
			doType(KeyEvent.VK_SHIFT, KeyEvent.VK_BACK_SLASH);
			break;
		case ';':
			doType(KeyEvent.VK_SEMICOLON);
			break;
		case ':':
			doType(KeyEvent.VK_SHIFT, KeyEvent.VK_SEMICOLON);
			break;
		case '\'':
			doType(KeyEvent.VK_QUOTE);
			break;
		case '"':
			doType(KeyEvent.VK_SHIFT, KeyEvent.VK_2);
			break;
		case ',':
			doType(KeyEvent.VK_COMMA);
			break;
		case '<':
			doType(KeyEvent.VK_LESS);
			break;
		case '.':
			doType(KeyEvent.VK_PERIOD);
			break;
		case '>':
			doType(KeyEvent.VK_GREATER);
			break;
		case '/':
			doType(KeyEvent.VK_SLASH);
			break;
		case '?':
			doType(KeyEvent.VK_SHIFT, KeyEvent.VK_SLASH);
			break;
		case ' ':
			doType(KeyEvent.VK_SPACE);
			break;
		default:
			throw new IllegalArgumentException("Cannot type character "
					+ character);
		}
	}

	private static void doType(int... keyCodes) {
		doType(keyCodes, 0, keyCodes.length);
	}

	private static void doType(int[] keyCodes, int offset, int length) {
		if (length == 0) {
			return;
		}

		robot.keyPress(keyCodes[offset]);
		robot.delay(2);
		doType(keyCodes, offset + 1, length - 1);
		robot.keyRelease(keyCodes[offset]);
	}

	public static void main(String[] args) {
		new Client().init();
	}
}
